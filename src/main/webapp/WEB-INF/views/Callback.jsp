<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link href="css/hpm2samplecode.css" rel="stylesheet" type="text/css" />
<title>Result</title>
<style>
	.firstTitle {
		width: 600px;
		margin: 80px auto 0px;
		font-size: 5;
	}

	.title {
		width: 600px;
		margin: 25px auto 0px;
	}

	.item {
		width: 600px;
		margin: 10px auto 0px;
	}

	#checkBoxDiv {
		/* Add any additional styles for the checkBoxDiv */
	}
</style>
<script type="text/javascript">

function hideBackToHomepage() {
	if(window.parent != window) {
		// Inline, submit button outside hosted page. Hide the 'Back To Homepage' button.
		document.getElementById("backToHomepage").style.display = "none";
	}	
}

if(window.parent != window) {
	// Inline, submit button outside hosted page.
	
	var responseFrom = request.getParameter("responseFrom");
	var success = request.getParameter("success");
	var newToken = ""; // Define newToken variable
	var newSignatureStr = ""; // Define newSignatureStr variable

	if ("Response_From_Submit_Page".equals(responseFrom) || "Response_From_3D_Validation".equals(responseFrom)) {
		if ("true".equals(success)) {
			// Submitting hosted page succeeded.
	
	window.parent.submitSucceed();

		} else {
			// Submitting hosted page failed.

	window.parent.submitFail("<%=request.getQueryString()%>", "<%=newToken%>", "<%=newSignatureStr%>");
	
		}
	}
	
}
</script>
</head>
<body onload="hideBackToHomepage()">
	<div class="firstTitle"><font size="4"><%=message%></font></div>
	<div id="backToHomepage" class="item"><button onclick='window.location.replace("Homepage.jsp")' style="margin-left: 150px; width: 140px; height: 24px;">Back To Homepage</button></div>
</body>
</html>
