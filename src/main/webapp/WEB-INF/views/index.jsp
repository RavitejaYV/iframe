<!DOCTYPE html>
<html lang="en">

<head>
  <title>Checkout Experience</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" />
  <link rel="icon" type="image/x-icon" href="https://www.estuate.com/wp-content/uploads/2020/07/favicon.ico">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.20/dist/sweetalert2.min.js"></script>
  <script src="https://common.olemiss.edu/_js/sweet-alert/sweet-alert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://common.olemiss.edu/_js/sweet-alert/sweet-alert.css">
</head>
  <style>
    .mainBody {
      background-color: #f5f5f5;
      height: 100%;
    }

    .normalText {
      font-weight: normal;
    }

    table {
      border: 1px solid #cccccc;
      border-radius: 15px;
      border-collapse: collapse;
      padding: 10px;
      margin-top: 25px;
      margin-bottom: 25px;
    }

    th {
      padding-left: 10px;
      padding-right: 10px;
      padding-top: 5px;
      padding-bottom: 5xp;
    }

    td {
      padding: 10px;
    }
  </style>
</head>

<body>
  <div class="container-fluid">
    <div class="row content">
      <h4>
        <div style="text-align: right; margin-right: 220px; padding: 10px">
          <img src="https://www.estuate.com/wp-content/uploads/2020/02/estuate-logo.png" alt="Estuate" id="logo" />
        </div>
      </h4>
      <hr style="margin-top: 0; margin-bottom: 0; border: 1px solid #e0dfdf" />
    </div>
    <div class="row content">
      <div style="padding-left: 200px; padding-right: 200px">
        <div class="col-sm-8 mainBody" style="padding: 10px; height: 700px">
          <form id="checkoutForm">
            <div class="form-row">
              <div class="form-group col-md-12" style="margin-top: 20px">
                <h4 style="float: right">
                  <small>Already a Customer? Log in </small>
                </h4>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>2. Cardholder Information</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label class="normalText" for="inputEmail4">First Name</label>
                <input type="text" class="form-control" id="inputEmail4" placeholder="First Name" />
              </div>
              <div class="form-group col-md-6">
                <label class="normalText" for="inputPassword4">Last Name</label>
                <input type="text" class="form-control" id="inputPassword4" placeholder="Last Name" />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6" style="margin-top: 20px">
                <label class="normalText" for="inputEmail4">Email</label>
                <input type="email" class="form-control" id="inputEmail4" placeholder="name@company.com" />
              </div>
              <div class="form-group col-md-6" style="margin-top: 20px">
                <label class="normalText" for="inputPassword4">Password</label>
                <input type="password" class="form-control" id="inputPassword4" placeholder="Password" />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12" style="margin-top: 20px">
                <label for="inputZip">3. Enter payment details</label>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label class="normalText" for="inputAddress">pageId</label>
                <input type="text" class="form-control" id="inputAddress" placeholder="pageId" />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6" style="margin-top: 20px">
                <label class="normalText" for="inputCity">method</label>
                <input type="text" class="form-control" id="inputCity" placeholder="method" />
              </div>

              <div class="form-group col-md-6" style="margin-top: 20px">
                <label class="normalText" for="inputZip">currency</label>
                <input type="text" class="form-control" id="inputZip" placeholder="currency" />
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12" style="margin-top: 30px">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="gridCheck" />
                  <label style="padding-left: 10px" class="form-check-label normalText" for="gridCheck">
                    <small>
                      I agree to the <a href="#">Terms of Service</a> and
                      <a href="#">Privacy Policy</a></small>
                  </label>
                </div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <button id="submitBtn" type="submit" class="btn btn-primary ">Submit</button>
              </div>
            </div>
          </form>
        </div>

        <div class="col-sm-4">
          <h4>Accepted payment types</h4>
          <img
            src="https://www.citypng.com/public/uploads/preview/visa-mastercard-discover-american-express-icons-21635415958rgxaunvs7z.png"
            width="200" />

          <table style="width: 100%">
            <tr>
              <th colspan="2">Order summary</th>
            </tr>
            <tr>
              <td>Item</td>
              <td style="text-align: right">Price</td>
            </tr>
            <tr>
              <th>Pay Annually Gold Plan</th>
              <th style="text-align: right">$999.99</th>
            </tr>
            <tr>
              <th>Setup Fee</th>
              <th style="text-align: right">$100.00</th>
            </tr>
            <tr>
              <td></td>
              <td style="text-align: right">
                <hr style="border: 1px solid #e0dfdf" /></td>
            </tr>
            <tr>
              <th>Total</th>
              <th style="text-align: right">$1099.99</th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script type="text/javascript"
              src="https://static.zuora.com/Resources/libs/hosted/1.3.1/zuora-min.js"></script>
  
 
  <script>
    // Wait for the DOM to be ready
    var resp;
    $(document).ready(function () {
      // Enable submit button only if the checkbox is checked
      $('#gridCheck').change(function () {
        $('#submitBtn').prop('disabled', !this.checked);
      });

      // Handle form submission
      $('#checkoutForm').submit(function (event) {
        event.preventDefault(); // Prevent form submission

        // Retrieve form values
        var firstName = $('#inputEmail4').val();
        var lastName = $('#inputPassword4').val();
        var email = $('#inputEmail4').val();
        var password = $('#inputPassword4').val();
        var pageId = $('#inputAddress').val();
       var method = $('#inputCity').val();
      //  var expirationParts = expirationDate.split('/');
      //  var expirationMonth = parseInt(expirationParts[0]);
      //  var expirationYear = parseInt(expirationParts[1]);
      //  expirationYear = 2000 + expirationYear;
        var currency = $('#inputZip').val();

        // Prepare the data object to be sent to the API
        var data = {
  
          
  pageId: pageId,
  //creditCardType: "Discover",
  method: "POST",
 // expirationYear: expirationYear,
  cirrency: "USD", 
  uri:"https://apisandbox.zuora.com/apps/PublicHostedPageLite.do"
};
// gatewayOptions: {

// }
        // Perform the API request
        $.ajax({
          url: 'http://localhost:8080/rsasignatures',
          type: 'POST',
          data: JSON.stringify(data),
          contentType: "application/json; charset=utf-8",
          success: function (response) {
            var result = JSON.stringify(response);
            //var resp = response.paymentMethodId;
            resp=response;
            var suc = response.success;
            
              //alert("suc : "+suc);
              //alert("dddg : "+resp);
              if(suc == true){
                swal("Payment Method Created Successfully", "Payment Id : "+resp, "success")
              }
              else{
                swal("Payment method failed to create")
              }
          },
          error: function (error) {
            // Handle API error here
            console.error('API error:', error);
            // Optionally, display an error message to the user
            alert('An error occurred. Please try again.');
          },
        });
      });
    });





  </script>
</body>

</html>