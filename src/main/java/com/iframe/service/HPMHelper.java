 package com.iframe.service;
 

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import javax.servlet.http.HttpServletRequest;

/**
 * HPM utility class which can 
 * 		1. load and maintain configurations
 * 		2. generate and validate signature
 * 		3. encrypt pre-populate fields
 *       
 * 
 */
public class HPMHelper {
	private static final String DELIM="#";
	//private static final int DEFAULT_HTTPS_PORT = 443;
	private static final Set<String> fieldToEncrypt = new HashSet<String>();
	
    static {
        fieldToEncrypt.add("creditCardNumber");
        fieldToEncrypt.add("cardSecurityCode");
        fieldToEncrypt.add("creditCardExpirationYear");
        fieldToEncrypt.add("creditCardExpirationMonth");
        fieldToEncrypt.add("bankAccountNumber");
        fieldToEncrypt.add("bankAccountName");
    }
	
	private static String url = "";
	private static String endPoint = "";
	private static String callbackURL = "";
	private static String username = "";
	private static String password = "";
	private static String publicKeyString = "";
	private static String jsPath = "";
	private static String jsVersion ="";
	private static Map<String, HPMPage> pages = new TreeMap<String, HPMPage>();
	 	
	public static String getJsPath() {
		return jsPath;
	}
	
	public static Map<String, HPMPage> getPages() {
		return pages;
	}
	
	public static HPMPage getPage(String pageName) {
		return pages.get(pageName);
	}
	
	public static String getJsVersion() {
		return jsVersion;
	}
	
	/**
	 * HPMPage contains the configurations for a single Hosted Page.
	 * 
	 * 
	 */
	public static class HPMPage {
		private String pageId;
		private String paymentGateway;
		private List<String> locales;
		
		public String getPageId() {
			return pageId;
		}
		
		public void setPageId(String pageId) {
			this.pageId = pageId;
		}
		
		public String getPaymentGateway() {
			return paymentGateway;
		}
		
		public void setPaymentGateway(String paymentGateway) {
			this.paymentGateway = paymentGateway;
		}
				
		public List<String> getLocales() {
			return locales;
		}

		public void setLocales(List<String> locales) {
			this.locales = locales;
		}

		public HPMPage() {
			pageId = "";
			paymentGateway = "";
			locales = new ArrayList<String>();			
		}		
	}
	
	/**
	 * Load HPM configuration file.
	 * 
	 * @param configFile - the HPM configuration file path
	 * @throws IOException
	 */
	public static void loadConfiguration(String configFile) throws IOException {
		Properties props = new Properties();
		props.load(new FileInputStream(configFile));
		
		url = props.getProperty("url");
		endPoint = props.getProperty("endPoint");
		callbackURL = props.getProperty("callbackURL");
		username =props.getProperty("username");
		password = props.getProperty("password");
		publicKeyString = props.getProperty("publicKey");
		jsPath = props.getProperty("jsPath");
		Pattern pattern = Pattern.compile(".+hosted/(.+)/zuora.+");
		Matcher matcher = pattern.matcher(jsPath);
		if(matcher.matches()) {
			jsVersion = matcher.group(1);
		}
		
		pages.clear();
		for(Object key : props.keySet()) {
			pattern = Pattern.compile("page\\.([^\\.]+)\\.([^\\.]+)");
			matcher = pattern.matcher((String)key);
			if(matcher.matches()) {
				String value = props.getProperty((String)key);
				
				String name = matcher.group(1);
				HPMPage page = pages.get(name);
				if(page == null) {
					page = new HPMPage();
					pages.put(name, page);
				}
				
				if("pageId".equals(matcher.group(2))) {
					page.setPageId(value);
				} else if("paymentGateway".equals(matcher.group(2))) {
					page.setPaymentGateway(value);
				} else if("locale".equals(matcher.group(2))) {
					List<String> locales = new ArrayList<String>();
					for(String locale : value.split(",")) {
						if(!"".equals(locale.trim())) {
							locales.add(locale.trim());
						}
					}
					
					page.setLocales(locales);
				}
			}
		}
	}
	


}