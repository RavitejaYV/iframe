 package com.iframe.service;

import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.util.Base64Utils;
 

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iframe.entity.Paymentmethod_entity_class;
import com.iframe.entity.RsaSignatureRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
@Component
@Service
public class Iframe_service {
	
	@Value("${oauth.clientId}")
    private String clientId;

    @Value("${oauth.clientSecret}")
    private String clientSecret;

    @Value("${oauth.tokenUrl}")
    private String tokenUrl;

    public String generateToken() {
        // Creating the REST client
        RestTemplate restTemplate = new RestTemplate();
        // Preparing the request URL and headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        // Preparing the request body
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", "client_credentials");
        requestBody.add("client_id", clientId);
        requestBody.add("client_secret", clientSecret);
        // Preparing the request entity
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(requestBody, headers);
        // Sending the POST request to the token URL
        ResponseEntity<TokenResponse> response = restTemplate.postForEntity(tokenUrl, requestEntity, TokenResponse.class);
        // Extracting the token from the response
        TokenResponse tokenResponse = response.getBody();
        if (tokenResponse != null) {
            return tokenResponse.getAccessToken();
        } else {
            throw new RuntimeException("Failed to generate token.");
        }
    }
	        
	  
    public ResponseEntity<?> createCreditCardPaymentMethod(Paymentmethod_entity_class creditCardDetails) {
    	
        // Generate OAuth token
        String accessToken = generateToken();
        // Creating the REST client
        RestTemplate restTemplate = new RestTemplate();
        // Preparing the request URL and headers
        String url = "https://rest.apisandbox.zuora.com/v1/payment-methods/credit-cards";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(accessToken);
//        ObjectMapper objectMapper = new ObjectMapper();
//        String requestBody;
//        try {
//            requestBody = objectMapper.writeValueAsString(creditCardDetails);
//            System.out.println("Request Body: " + requestBody.toString()
//            );
//        } catch (JsonProcessingException e) {
//            System.out.println("Error converting request body to JSON: " + e.getMessage());
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to create credit card payment method.");
//        }
        // Preparing the request body
        HttpEntity<Paymentmethod_entity_class> requestEntity = new HttpEntity<>(creditCardDetails, headers);
        // Sending the POST request to Zuora API
        System.out.println(requestEntity.toString());
        ResponseEntity<?> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Object.class);
        return responseEntity;
    }

    public ResponseEntity<?> generateRsaSignature(RsaSignatureRequest request) {
        String url = "https://rest.apisandbox.zuora.com/v1/rsa-signatures";
       String uri= request.getUri();
       String method= request.getMethod();
       String pageId= request.getPageId();
        // Create the JSON payload
        JSONObject requestObject = new JSONObject();
        requestObject.put("uri", uri);
        requestObject.put("method", method);
        requestObject.put("pageId", pageId);
        String requestBody = requestObject.toString();
        String accessToken = generateToken();

        System.out.println(accessToken);
        
         HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(accessToken);
        // Create the request entity
        HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);
        // Send the request and get the response
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<?> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Object.class);
      //  Object responseBody = responseEntity.getBody();
        return responseEntity;
        
    }


}
