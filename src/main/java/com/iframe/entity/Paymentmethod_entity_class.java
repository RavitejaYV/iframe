package com.iframe.entity;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Paymentmethod_entity_class {
  
  private String accountKey;
  @OneToOne
  private CardHolderInformation cardHolderInfo;
  private String creditCardNumber;
  private String creditCardType;
  private boolean defaultPaymentMethod;
  private int expirationMonth;
  private int expirationYear;
  @OneToOne
  private GateWayOptions gatewayOptions;
  private int numConsecutiveFailures;
  private String securityCode;
public String getAccountKey() {
	return accountKey;
}
public void setAccountKey(String accountKey) {
	this.accountKey = accountKey;
}
public CardHolderInformation getCardHolderInfo() {
	return cardHolderInfo;
}
public void setCardHolderInfo(CardHolderInformation cardHolderInfo) {
	this.cardHolderInfo = cardHolderInfo;
}
public String getCreditCardNumber() {
	return creditCardNumber;
}
public void setCreditCardNumber(String creditCardNumber) {
	this.creditCardNumber = creditCardNumber;
}
public String getCreditCardType() {
	return creditCardType;
}
public void setCreditCardType(String creditCardType) {
	this.creditCardType = creditCardType;
}
public boolean isDefaultPaymentMethod() {
	return defaultPaymentMethod;
}
public void setDefaultPaymentMethod(boolean defaultPaymentMethod) {
	this.defaultPaymentMethod = defaultPaymentMethod;
}
public int getExpirationMonth() {
	return expirationMonth;
}
public void setExpirationMonth(int expirationMonth) {
	this.expirationMonth = expirationMonth;
}
public int getExpirationYear() {
	return expirationYear;
}
public void setExpirationYear(int expirationYear) {
	this.expirationYear = expirationYear;
}
public GateWayOptions getGatewayOptions() {
	return gatewayOptions;
}
public void setGatewayOptions(GateWayOptions gatewayOptions) {
	this.gatewayOptions = gatewayOptions;
}
public int getNumConsecutiveFailures() {
	return numConsecutiveFailures;
}
public void setNumConsecutiveFailures(int numConsecutiveFailures) {
	this.numConsecutiveFailures = numConsecutiveFailures;
}
public String getSecurityCode() {
	return securityCode;
}
public void setSecurityCode(String securityCode) {
	this.securityCode = securityCode;
}
  
  
  
  
}