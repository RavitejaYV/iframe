package com.iframe.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RsaSignatureRequest {
     @JsonProperty
    private String currency;
     @JsonProperty
    private String method;
     @JsonProperty
    private String pageId;
     @JsonProperty
    private String uri;
    // Getters and setters
    
    public String getCurrency() {
        return currency;
    }
    public RsaSignatureRequest(String currency, String method, String pageId, String uri) {
        super();
        this.currency = currency;
        this.method = method;
        this.pageId = pageId;
        this.uri = uri;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getMethod() {
        return method;
    }
    public void setMethod(String method) {
        this.method = method;
    }
    public String getPageId() {
        return pageId;
    }
    public void setPageId(String pageId) {
        this.pageId = pageId;
    }
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }
}