package com.iframe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.iframe.entity.Paymentmethod_entity_class;
import com.iframe.entity.RsaSignatureRequest;
//import com.ZuoraPaymentMethod.entity_class.Rsa_Signature;
import com.iframe.service.Iframe_service;

@Controller
public class Iframe_Controller {
	
	@Autowired
	private Iframe_service paymentService;
	
	@GetMapping("/payment_Hpme_page")
    public ModelAndView homepage() 
	{
    	ModelAndView model=new ModelAndView("index");
		//System.out.print("------");
	    return model;
	    
	}
	
	
	
	
	@GetMapping("/K")
    public ModelAndView homepage2() 
	{
    	ModelAndView model=new ModelAndView("rsa");
		//System.out.print("------");
	    return model;
	    
	}
	
	@GetMapping("/iframepage")
    public ModelAndView homepage3() 
	{
    	ModelAndView model=new ModelAndView("iframe");
		//System.out.print("------");
	    return model;
	    
	}

	@PostMapping("/creditcard")
	public ResponseEntity<?> createCreditCardPaymentMethod(@RequestBody Paymentmethod_entity_class creditCardDetails) throws Exception {
		
	    ResponseEntity<?> responseEntity = paymentService.createCreditCardPaymentMethod(creditCardDetails);
	    
	    if (responseEntity.getStatusCode().is2xxSuccessful()) {
	        return ResponseEntity.ok(responseEntity.getBody());
	    } else {
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to create credit card payment method.");
	    }

	}
	
    @PostMapping("/rsasignatures")
    public ResponseEntity<?> generateRsaSignature(@RequestBody RsaSignatureRequest request) {
    	//System.out.println(1);
        ResponseEntity<?> response = paymentService.generateRsaSignature(request);
        //System.out.println(2);
     
	        return response;
	    
	}
    
}
